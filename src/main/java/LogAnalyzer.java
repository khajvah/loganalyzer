import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;


public class LogAnalyzer extends Configured implements Tool {

    public static class MyMapper extends Mapper<LongWritable, Text, LongWritable, Text> {

        @Override public void map(final LongWritable key, final Text value, final Context context)
                throws IOException, InterruptedException {

            final String line = value.toString();

            context.write(key, new Text(line));
        }
    }


    public static class MyReducer extends Reducer<LongWritable, Text, LongWritable, Text> {

        @Override
        protected void reduce(final LongWritable key, final Iterable<Text> values, final Context context)
                throws IOException, InterruptedException {

            for (Text s: values) {
                final String line = s.toString();
                String[] tokenized_line = line.split("\\s");
                for (String l: tokenized_line) {
                    if (l.equals("Error:")) {
                        context.write(key, s);
                    }
                }
            }
        }
    }


    public int run(final String[] args) throws Exception {
        final Configuration conf = this.getConf();
        final Job job = Job.getInstance(conf, "Word Count");
        job.setJarByClass(LogAnalyzer.class);

        job.setMapperClass(MyMapper.class);
        job.setReducerClass(MyReducer.class);

        job.setMapOutputKeyClass(LongWritable.class);
        job.setMapOutputValueClass(Text.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        return job.waitForCompletion(true) ? 0 : 1;
    }

    public static void main(String[] args) {
        try {
            final int returnCode = ToolRunner.run(new Configuration(), new LogAnalyzer(), args);
            System.exit(returnCode);
        }
        catch (Exception e) {
            System.exit(2);
        }
    }
}
